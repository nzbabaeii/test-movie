import requests
from rest_framework.response import Response
from rest_framework.views import APIView
from urllib.parse import quote


class MovieListAPIView(APIView):
    def get(self, request,page_id, format=None):
        page = request.query_params.get('page',page_id )
        api_url = f'http://moviesapi.ir/api/v1/movies?page={page}'

        response = requests.get(api_url)

        if response.status_code == 200:
            data = response.json()
            return Response(data)
        else:
            return Response({"error": "Failed to fetch data from the API"}, status=response.status_code)




class MovieSearchAPIView(APIView):
    def get(self, request, *args, **kwargs):
        name = kwargs['name']
        
        page = request.query_params.get('page', kwargs['page_id'])
        
        name = quote(name)

        api_url = f'http://moviesapi.ir/api/v1/movies?q={name}&page={page}'

        response = requests.get(api_url)

        if response.status_code == 200:
            data = response.json()
            return Response(data)
        else:
            return Response({"error": "Failed to fetch data from the API"}, status=response.status_code)
