from django.contrib import admin
from django.urls import path ,include
from .views import *


urlpatterns = [
    path('films/<int:page_id>/',  MovieListAPIView.as_view()),
    path('search/<str:name>/<int:page_id>/',MovieSearchAPIView.as_view()),
]
